import Page from "@/components/Page/Page";

export default function Home() {
  return (
    <main className="app">
      <Page />
    </main>
  );
}
