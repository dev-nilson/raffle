import styles from "./Modal.module.css";

type ModalProps = {
  setIsOpen: (isOpen: boolean) => void;
  children: React.ReactNode;
};

export default function Modal({ children, setIsOpen }: ModalProps) {
  return (
    <div className={styles.backdrop} onClick={() => setIsOpen(false)}>
      <div className={styles.modal} onClick={(e) => e.stopPropagation()}>
        {children}
      </div>
    </div>
  );
}
