"use client";
import styles from "./Range.module.css";

type RangeProps = {
  imageNumber: number;
  setImageNumber: (imageNumber: number) => void;
};

export default function Range({ imageNumber, setImageNumber }: RangeProps) {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setImageNumber(Number(e.target.value));
  };

  return (
    <input
      className={styles.range}
      type="range"
      min="1"
      max="37"
      onChange={handleChange}
      value={imageNumber}
    />
  );
}
