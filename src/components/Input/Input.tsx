import { InputHTMLAttributes } from "react";
import styles from "./Input.module.css";

type InputProps = InputHTMLAttributes<HTMLInputElement> & {
  label?: string;
};

export default function Input({ label, ...props }: InputProps) {
  return (
    <div className={styles.group}>
      {label && <label className={styles.label} htmlFor={label}>{label}</label>}
      <input id={label} className={styles.input} {...props} />
    </div>
  );
}
