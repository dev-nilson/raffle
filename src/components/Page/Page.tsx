"use client";
import { useState } from "react";
import Image from "next/image";
import { SubmitHandler, useForm } from "react-hook-form";
import Range from "../Range/Range";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import Input from "../Input/Input";
import styles from "./Page.module.css";

interface IFormInput {
  name: String;
  phone: String;
}

export default function Page() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormInput>();
  const [imageNumber, setImageNumber] = useState(1);
  const [isOpen, setIsOpen] = useState(false);
  const onSubmit: SubmitHandler<IFormInput> = (data) => console.log(data);

  return (
    <>
      <div className={styles.banner}>
        <div className={styles.head}>
          <h1 className={styles.title}>Jordan 1 Mid Lakers</h1>
          <p className={styles.subtitle}>8.5 US | 26.5 CM</p>
          <Button onClick={() => setIsOpen(true)}>Reservar Ticket</Button>
        </div>
        <div className={styles.body}>
          <Image
            className={styles.image}
            src={`/image${imageNumber}.jpg`}
            alt="jordan 1 mid lakers"
            width="800"
            height="500"
            sizes="(max-width: 768px) 100vw, 50vw"
          />
          <div className={styles.controls}>
            <Range imageNumber={imageNumber} setImageNumber={setImageNumber} />
          </div>
        </div>
      </div>
      {isOpen && (
        <Modal setIsOpen={setIsOpen}>
          {/* <form className={styles.modal} onSubmit={handleSubmit(onSubmit)}>
            <input
              placeholder="Jose Hernandez"
              {...(register("name"),
              {
                required: true,
                minLength: 2,
                maxLength: 40,
              })}
            />
            {errors.name && <p>Nombre es obligatorio</p>}
            <input
              id={"phone"}
              className={styles.input}
              placeholder="1234-5678"
              {...register("phone", {
                required: true,
                pattern: /^[0-9]{4}-[0-9]{4}$/,
              })}
            />
            {errors.phone && <p>Telefono es obligatorio</p>}
            <Button>Confirmar</Button>
          </form> */}
        </Modal>
      )}
    </>
  );
}
